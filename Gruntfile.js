module.exports = function(grunt) {
	grunt.initConfig({

		jshint: {
			src: {
				options: {
					jshintrc: true
				},
				src: ['src/index.js']
			},
			options: {
				reporter: require('jshint-stylish'),
				strict: true
			}
		},
		uglify: {
			build: {
				files: {
					'dist/index.js': ['src/index.js']
				}
			}
		}
	});

	// *** Plugins ***
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	// *** Tasks ***
	grunt.registerTask('build', ['jshint', 'uglify']);
};