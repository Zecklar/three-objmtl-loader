> NodeJS wrapper for Three.js' OBJMTLLoader

Three-objmtl-loader loads a Wavefront .obj file with materials. It's a NodeJS warpper so it can be used with npm.

### Install

The module is not in npm repository (yet). You need to download the repository content and extract it to under node_modules directory of your project.
After that run
```
$ npm install node_modules/three-objmtl-loader
```
NOTE: Remember to delete .git folder if it exists.

### Build
To build the project, Grunt is needed to be installed globally. After that, just run
```
$ grunt build
```

### Usage

```js
var THREE = require('three');
var OBJMTLLoader = require('three-objmtl-loader');
OBJMTLLoader(THREE);

var objmtlLoader = new THREE.OBJMTLLoader();
objmtlLoader.load('path/to/object.obj', 'path/to/material.mtl', ...);
```

### License

ISC